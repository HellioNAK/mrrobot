### Quick summary ###
mrrobot.py is a dictionary making tool.
The logic is simple, the script takes in parameters from the user.
The script then outputs all the possible combination of these parameters into a file.
The file may then be used as a wordlist.

### Version ###
1.04

### How do I get set up? ###

This was made on python 2.7.9.
It would probably run on anything 2.7.x, I haven't tested it.

### Code flow ###
First thing the script does is reading the flags.
The mandatory flag is obviously -p, without that you won't logically have anything to build.

The parameters are sorted into a dictionary in the sense of 
{ Length : [parameter1,parameter2], Length : [parameter3,parameter4]}
For example:

-p 1 2 3 4 ab ac ad troll gandalf
Would produce the following dictionary:
{ 1:['1','2','3','4'], 2:['ab','ac','ad'], 5:['troll'], 7:['gandalf'] }

The next phase is making a file that contains all the possiblities of joining these up from a certain minimum length (user defined) to a certain maximum length (user defined).

The list would yeild the following results for the dictionary above (password length 5 to 7 characters):


```
#!python

1,1,1,1,1
1,1,1,2
1,1,2,1
1,2,1,1
1,2,2
2,1,1,1
2,1,2
2,2,1
5
1,1,1,1,1,1
1,1,1,1,2
1,1,1,2,1
1,1,2,1,1
1,1,2,2
1,2,1,1,1
1,2,1,2
1,2,2,1
1,5
2,1,1,1,1
2,1,1,2
2,1,2,1
2,2,1,1
2,2,2
5,1
1,1,1,1,1,1,1
1,1,1,1,1,2
1,1,1,1,2,1
1,1,1,2,1,1
1,1,1,2,2
1,1,2,1,1,1
1,1,2,1,2
1,1,2,2,1
1,1,5
1,2,1,1,1,1
1,2,1,1,2
1,2,1,2,1
1,2,2,1,1
1,2,2,2
1,5,1
2,1,1,1,1,1
2,1,1,1,2
2,1,1,2,1
2,1,2,1,1
2,1,2,2
2,2,1,1,1
2,2,1,2
2,2,2,1
2,5
5,1,1
5,2
7


```

The next procedure would read this file one line at a time and replace the numeric placeholders with their corresponding value in the dictionary built above.

1 would be replaced with each value from the corresponding list (['1','2','3'])
2 would be replaced with each value from the corresponding list (['ab','ac','ad'])
... and so on

Each combination will be saved into the output file.
Cleanup would then be performed to delete the MRROBOT.tmp file.