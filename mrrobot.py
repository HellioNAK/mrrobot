#Made by Asaf Katz
#Current version is dated at 22/12/2015
#Version 1.05
#Flag support using the argparse module.
#Version 1.06
#Added color support
################
# Dependancies:#
#Colorama      #
################
import argparse,os
from time import sleep
from time import time
try:
	import colorama	
	colorama.init()
	def getColor(c):
		colors={'white' : '\e[1;37m',
		'dgray' : '\x1b[90m',
		'DGRAY' : '\x1b[100m',
		'lred' : '\x1b[91m',
		'LRED' : '\x1b[101m',
		'lgreen' : '\x1b[92m',
		'LGREEN' : '\x1b[102m',
		'lyellow' : '\x1b[93m',
		'LYELLOW' : '\x1b[103m',
		'lblue' : '\x1b[94m',
		'LBLUE' : '\x1b[104m',
		'lmagneta' : '\x1b[95m',
		'LMAGENTA' : '\x1b[105m',
		'lcyan' : '\x1b[96m',
		'LCYAN' : '\x1b[106m',
		'lgray' : '\x1b[97m',
		'LGRAY' : '\x1b[107m'}
		return colors.get(c,"")
except:
	COLORAMA=False
	def getColor(c):
		return ""

parser = argparse.ArgumentParser(prog="mrrobot.py",
                                 description="%sHellioNAK's%s tool. Custom made dictionary. Made as a tribute to Mr.Robot."%(getColor('lred'),getColor('lblue')),
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 epilog="""Examples:

python mrrobot.py -p best dict ever 1337 x o
python mrrobot.py -nod -o 4.txt -p Sir Donnie a b c 1 2 3 4
python mrrobot.py -w -o 4.txt -p 3 4 5 6 -m 6 -M 10""")
parser.add_argument('-p',dest="PARGS", default=[],help="Parameters for the password.",nargs="+",required=True)
parser.add_argument('-m',dest="MINLENGTH",type=int,default=6,help="Set min password length. Default: 6")
parser.add_argument('-M',dest="MAXLENGTH",type=int,default=8,help="Set max password length. Default: 8")
parser.add_argument('-w',action="store_true",dest="MODE",default=False, help="Overwrite file mode. Default: Append to file.")
parser.add_argument('-o',dest="FILENAME",default='_wordlist_.txt',help="Output to filename. Default: _wordlist_.txt")
parser.add_argument('--must-contain',dest="MUST_LIST",default=[],help="Parameters that must exist in the passwords",nargs="+")
parser.add_argument('--prefix',dest="PREFIX",type=str,default="",help="Add prefix to every password.")
parser.add_argument('--suffix',dest="SUFFIX",type=str,default="",help="Add suffix to every password.")
parser.add_argument('--output',dest="FILENAME",default='_wordlist_.txt')
parser.add_argument('--nod',action="store_true",dest="NOONLYDIGITS",default=False, help="Exclude only-digit passwords.")
parser.add_argument('-v',action="store_true",dest="VERBOSE",default=False,help="Verbose.")

#vars is used to make the parse_args() output into a dictionary format
options = vars(parser.parse_args())

def sort_params(s):
#Makes a dictionary {Length:[List Params with that length]}
    t1={}
    t2=[]
    s.sort(key=len)
    prev=len(s[0])
    for i in list(s):
        item=s.pop(s.index(i))
        clen=len(item)
        if (prev<clen):
            t1[prev]=t2
            prev=clen
            t2=[item]
        else:
            t2.append(item)
    if (len(t2)>=1 or len(t1)==0):
        t1[clen]=t2
    return t1
def write_to_file(writePassword):
    global mustSwitch,MUST_LIST
    if mustSwitch:
        removeIndexes=[]
        for passIndex in range(len(writePassword)):
            isIn=False
            for mustBeUsed in MUST_LIST:
                if mustBeUsed in writePassword[passIndex]:
                    isIn=True
            if not isIn:
                removeIndexes.append(passIndex)
        removeIndexes.reverse()
        for i in removeIndexes:
            del(writePassword[i])
    if PREFIX!="" or SUFFIX!="":
        newWritePassword = [PREFIX+pw+SUFFIX for pw in writePassword]
        writePassword=newWritePassword
    if (VERBOSE):
        for writing in writePassword:
            print("[+] %s"%writing)
    writePassword="\n".join(writePassword)
    WORDLIST.write("%s\n"%writePassword)
def checkOnlyDigits(password):
    checkdigits=False
    for character in password:
        if character not in DIGITS:
            checkdigits=True
    return checkdigits
def makeDictionary(RemainingPossi,pDictionary,incrementedPassword):
    global CachePassword
    if len(CachePassword)>=1000:
        write_to_file(CachePassword)
        CachePassword=[]
    if len(RemainingPossi)==0 or RemainingPossi[0]=="":
        if not NOONLYDIGITS:
            CachePassword.append(incrementedPassword)
        else:
            if checkOnlyDigits(incrementedPassword[:len(incrementedPassword)-1]):
                CachePassword.append(incrementedPassword)
    else:
        for possibility in pDictionary[int(RemainingPossi[0])]:
            makeDictionary(RemainingPossi[1:],pDictionary,incrementedPassword+possibility)
def getPossibilities():
    #Gets a {dictionary}, puts list containing the keys into the global variable POSSI.
    global PDICT, TMPFILE
    APPEND='a+'
    for key in PDICT:
        POSSI.append(key)
    if (os.path.exists(TMPFILENAME)):
        print "[+] %s already exists. Removing."%TMPFILENAME
        os.remove(TMPFILENAME)
    TMPFILE=open(TMPFILENAME,APPEND)
    for i in range(MINLENGTH,MAXLENGTH+1,1):
        makePossibilities([],0,i)
    TMPFILE.close()
def makePossibilities(incrementedOptions,currentLength,passwordLength):
    global POSSI
    if (currentLength==passwordLength):
        m=",".join(incrementedOptions)
        TMPFILE.write("%s\n"%m)
    elif (currentLength>passwordLength):
        pass
    else:
        remainingLength=passwordLength-currentLength
        for possibility in POSSI:
            if (possibility<=remainingLength):
                forkedOptions=list(incrementedOptions)
                forkedOptions.append(str(possibility))
                makePossibilities(list(forkedOptions),currentLength+int(possibility),passwordLength)
def fileExistsHandler():
    if (os.path.exists(FILENAME) and not(os.path.isdir(FILENAME))):
        try:
            q=raw_input("[!] File exists. [y] to proceed > ")
            if (q.lower()!="y"):
                print("[-] Exiting.")
                print("[-] Canceled.")
                exit()
            else:
                modeRelated="Appending to"
                if (MODE=="w"):
                    modeRelated="Using"
                print("[+] %s existing file(%s)."%(modeRelated,FILENAME))
        except (EOFError,UnboundLocalError):
            KBInterruptMsg()
        except UnboundLocalError:
            pass
        except Exception as e:
            print "Unexpected Error "+e
def cleanUp():
    try:
        TMPFILE.close()
        os.remove(TMPFILENAME)
    except Exception as e:
        print "\n[!] Cleanup failed."
        print "[!] Unexpected Error: ", e
def KBInterruptMsg():
    print "[-] Keyboard Interruption.\n[-] Exiting."
def makeWordList():
    global WORDLIST, CachePassword
    CachePassword=[]
    READ='r'
    WORDLIST=open(FILENAME,MODE)
    TMPFILE=open(TMPFILENAME,READ)
    while(1):
        temp=TMPFILE.readline()
        if temp=="":
            break;
        temp=temp.rstrip().split(",")
        makeDictionary(temp,PDICT,"")
    if (len(CachePassword)>0):
        write_to_file(CachePassword)
    TMPFILE.close()
    WORDLIST.close()
def information():
    values=[]
    for key in PDICT:
        values+=PDICT[key]
    values=", ".join(values)
    print "[+] Paramters being used:\n[+] %s\n[+] Creation will begin in 10 seconds."%values
    sleep(10)

##### Options setup #####
DIGITS="0123456789"
MINLENGTH=options['MINLENGTH']
MAXLENGTH=options['MAXLENGTH']
FILENAME=options['FILENAME']
NOONLYDIGITS=options['NOONLYDIGITS']
VERBOSE=options['VERBOSE']
PARGS=options['PARGS']
PDICT=sort_params(list(PARGS))
TMPFILENAME="MRROBOT.tmp"
POSSI=[]
PREFIX=str(options['PREFIX'])
SUFFIX=str(options['SUFFIX'])
MUST_LIST=options['MUST_LIST']
mustSwitch=False
if (options['MODE']):
    MODE='w'
else:
    MODE='a'
if (len(MUST_LIST)>0):
    mustSwitch=True

##### Main #####
def main():
    global TMPFILENAME, MINLENGTH, MAXLENGTH, FILENAME, PDICT,POSSI,TMPFILE,VERBOSE,PREFIX,SUFFIX
    fileExistsHandler()
    if VERBOSE:
        information()
    start_time=time()
    getPossibilities()
    makeWordList()
    os.remove(TMPFILENAME)
    print "[+] Done."
    print "\nExecution Time: %s"%(time()-start_time)

try:
    main()
except KeyboardInterrupt as e:
    KBInterruptMsg()
    cleanUp()
except UnboundLocalError as e:
    print e
except Exception as e:
    print e