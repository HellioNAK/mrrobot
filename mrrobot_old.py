import sys
import os

global TMPFILENAME
TMPFILENAME="MRROBOT.tmp"

def write_to_file(m):
    if NOONLYDIGITS:
        checkdigits=False
        for i in m:
            if i not in DIGITS:
                checkdigits=True
        if (checkdigits):
            WORDLIST.write("%s\n"%m)
    else:
        WORDLIST.write("%s\n"%m)



def mkdic(l,d,a):
    if len(l)==0 or l[0]=="":
        if (v):
            print("[+] Added: %s"%a);
        #WORDLIST.write(a+"\n")
        write_to_file(a);
    else:
        for k in d[int(l[0])]:
            mkdic(l[1:],d,a+k)
def check_no_only_digits(s):
    flag="-noonlydigits"
    if (flag in s):
        s.remove(flag)
        return True;
    return False;
def check_mode(s):
    flag = '-w'
    if (flag in s):
        s.remove(flag)
        return "w+"
    return 'a'
def check_verbose(s):
    flag = "-v"
    if (flag) in s:
        s.remove(flag)
        return True
    return False
def check_file(s):
    flag='-o'
    if (flag in s):
        fname=s[s.index(flag)+1]
        s.remove(fname)
        s.remove(flag)
        return fname
    return "__passwords__.txt"
def check_help(s):
    flag = '-h'
    if (flag in s) or (len(s)<2):
        msg="""Welcome to SirDonnie's tool, imitating Mr.Robot's dictionary maker.
        Mandatory Flags:
        [-m] - Minimum password length.
        [-M] - Maxmimum password length.
        [-p] - Parameters to use.
        Optional Flags:
        [-w] - Overwrite to file mode. (default is append)
        [-o WORDLIST] - Write to WORDLIST. (Default WORDLIST is __passwords__.txt)
        [-h] - Display this message.
        [-v] - Add verbose.
        [-nodigitsonly] - Make sure the entries do not contain only-digit[0-9] passwords.

        Example:
        mrrobot.py -w -o 4.txt -p 3 4 5 6 -m 6 -M 10
"""
        print msg
        exit()
def check_minlength(s):
    flag="-m"
    min=0
    if flag in s:
        min=s[s.index(flag)+1]
        del s[s.index(flag)+1]
        s.remove(flag)
    return int(min)
def check_maxlength(s):
    flag="-M"
    if flag in s:
        max=s[s.index(flag)+1]
        del s[s.index(flag)+1]
        s.remove(flag)
        return int(max)
    else:
        print "[!] Missing flag [-M]."
        print "[-] Exiting."
        exit()
def check_params(s):
    flag = '-p'
    if flag in s:
        return s[s.index(flag)+1:]
    else:
        print ("[!] Missing %s flag\n[-] Exiting."%flag)
        exit()
def getPossibilities(s):
    #Gets a {dictionary}, puts list containing the keys into the global variable POSSI.
    POSSI=[]
    for key in s:
        POSSI.append(key)
    return POSSI;
def recu(a,b,thelength):
    if (b==thelength):
        m=",".join(a);
        TMPFILE.write("%s\n"%m)
    elif (b>thelength):
        pass
    else:
        sendFurther(a,b,thelength)
def sendFurther(a,b,thelength):
    t=thelength-b
    for i in POSSI:
        if (i<=t):
            v=list(a)
            v.append(str(i))
            recu(list(v),b+int(i),thelength)
def sort_params(s):
#Makes a dictionary Length:[List Params with that length]
    t1={}
    t2=[]
    s.sort(key=len)
    prev=len(s[0])
    for i in list(s):
        item=s.pop(s.index(i))
        clen=len(item)
        if (prev<clen):
            t1[prev]=t2
            prev=clen
            t2=[item]
        else:
            t2.append(item)
    if (len(t2)>=1 or len(t1)==0):
        t1[clen]=t2
    return t1
def main():
    global MINLENGTH,MAXLENGTH,v,WORDLIST,POSSI,TMPFILE, POSSI,NOONLYDIGITS, DIGITS
    DIGITS="0123456789"
    s=sys.argv[1:]
    check_help(s)
    [str(x) for x in s]
    MINLENGTH=check_minlength(s)
    MAXLENGTH=check_maxlength(s)
    MODE=check_mode(s)
    FILENAME=check_file(s)
    NOONLYDIGITS=check_no_only_digits(s)
    v=check_verbose(s)
    s=check_params(s)
    s.sort()
    sdict=sort_params(s)
    POSSI = getPossibilities(sdict)
    TMPFILE=open(TMPFILENAME,'a+')
    for i in range(MINLENGTH,MAXLENGTH+1,1):
        recu([],0,i)
    TMPFILE.close()
    if (os.path.exists(FILENAME) and not(os.path.isdir(FILENAME))):
        try:
            q=raw_input("[!] File exists. [y] to proceed > ")
        except Exception:
            cleanUp()
        if (q.lower()!="y"):
            print("[-] Exiting.")
            exit()
        else:
            print("[+] ok.")
    WORDLIST=open(FILENAME,MODE)
    TMPFILE=open(TMPFILENAME,'r')
    while(1):
        temp=TMPFILE.readline()
        if temp=="":
            break;
        temp=temp.rstrip().split(",")
        mkdic(temp,sdict,"")
    TMPFILE.close()
    WORDLIST.close()
    os.remove(TMPFILENAME)
    print "[+] Done."
def cleanUp():
    try:
        TMPFILE.close()
        os.remove(TMPFILENAME)
    except Exception as e:
        print "\n[!] Cleanup failed."
        print "[!] Unexpected Error: ", e
try:
    main()
except KeyboardInterrupt as e:
    print "\n[-] Keyboard interruption.\n[-] Exiting."
    cleanUp()